import psycopg2
from sklearn import linear_model
import numpy as np

myConnection = psycopg2.connect( host='172.16.44.74',port = '5432', user='postgres', password='Zima789!', dbname='ptcl' )
cur = myConnection.cursor()

#........ DATA_PROCESSING...............

#listing all the labels for the model
cur.execute("SELECT SUM(CURR_BILL), SUM(BROADBAND_BILL), SUM(PSTN_BILL), SUM(IPTV_BILL), name_displ "
            "from tb_ptcl_dp_complaints,abbottabad_areas_v1 "
            "WHERE ST_Contains(abbottabad_areas_v1.geom,tb_ptcl_dp_complaints.geom) "
            "GROUP BY name_displ")
# labels
CURR_BILL=[]
BROADBAND_BILL=[]
PSTN_BILL=[]
IPTV_BILL=[]
name_displ=[]

#label vectors
for firstname in cur.fetchall():
    CURR_BILL.append(firstname[0])
    BROADBAND_BILL.append(firstname[1])
    PSTN_BILL.append(firstname[2])
    IPTV_BILL.append(firstname[3])
    name_displ.append(firstname[4])

#listing all the features for the model
cur.execute("SELECT SUM(CASE WHEN abbottabad_points_v1.income_lev IN ('Very Low Income') THEN 1 ELSE 0 END) AS V_L_Income, "
            "SUM(CASE WHEN abbottabad_points_v1.income_lev IN ('Low Income') THEN 1 ELSE 0 END) AS L_Income, "
            "SUM(CASE WHEN abbottabad_points_v1.income_lev IN ('Medium Income') THEN 1 ELSE 0 END) AS M_Income, "
            "SUM(CASE WHEN abbottabad_points_v1.income_lev IN ('High Income') THEN 1 ELSE 0 END) AS H_Income, "
            "SUM(CASE WHEN abbottabad_points_v1.income_lev IN ('Very High Income') THEN 1 ELSE 0 END) AS V_H_Income, "
            "male_popul, female_pop, total_popu, no_of_hous, name_displ "
            "from abbottabad_areas_v1, abbottabad_points_v1 "
            "WHERE ST_Contains(abbottabad_areas_v1.geom,abbottabad_points_v1.geom) "
            "GROUP BY name_displ, male_popul, female_pop, total_popu, no_of_hous, name_displ")

#features
Very_Low_Income = []
Low_Income = []
Medium_Income = []
High_Income = []
Very_High_Income = []
male_popul = []
female_pop = []
total_popu = []
no_of_hous = []
name_displ_1 = []

#testing features
new_Very_Low_Income = []
new_Low_Income = []
new_Medium_Income = []
new_High_Income = []
new_Very_High_Income = []
new_male_popul = []
new_female_pop = []
new_total_popu = []
new_no_of_hous = []
new_name_displ_1 = []

#feature vectors
for firstname in cur.fetchall():
    Very_Low_Income.append(firstname[0])
    Low_Income.append(firstname[1])
    Medium_Income.append(firstname[2])
    High_Income.append(firstname[3])
    Very_High_Income.append(firstname[4])
    male_popul.append(firstname[5])
    female_pop.append(firstname[6])
    total_popu.append(firstname[7])
    no_of_hous.append(firstname[8])
    name_displ_1.append(firstname[9])

#discarding areas in which donot have label vectors for example missing PTCL DP data in areas.
temp = []
test = []
for i in name_displ_1:
    if i not in name_displ:
        temp.append(name_displ_1.index(i))
        test.append(name_displ_1.index(i))
temp.sort(reverse=True)
for ii in temp:
    new_Very_Low_Income.append(Very_Low_Income[ii])
    new_Low_Income.append(Low_Income[ii])
    new_Medium_Income.append(Medium_Income[ii])
    new_High_Income.append(High_Income[ii])
    new_Very_High_Income.append(Very_High_Income[ii])
    new_male_popul.append(male_popul[ii])
    new_female_pop.append(female_pop[ii])
    new_total_popu.append(total_popu[ii])
    new_no_of_hous.append(no_of_hous[ii])
    new_name_displ_1.append(name_displ_1[ii])



    Very_Low_Income.pop(ii)
    Low_Income.pop(ii)
    Medium_Income.pop(ii)
    High_Income.pop(ii)
    Very_High_Income.pop(ii)
    male_popul.pop(ii)
    female_pop.pop(ii)
    total_popu.pop(ii)
    no_of_hous.pop(ii)
    name_displ_1.pop(ii)

#Data aLign
name_displ,CURR_BILL,BROADBAND_BILL,PSTN_BILL,IPTV_BILL = zip(*sorted(zip(name_displ,CURR_BILL,BROADBAND_BILL,PSTN_BILL,IPTV_BILL)))
name_displ_1,Very_Low_Income,Low_Income,Medium_Income,High_Income,Very_High_Income,male_popul,female_pop,total_popu,no_of_hous = zip(*sorted(zip(name_displ_1,Very_Low_Income,Low_Income,Medium_Income,High_Income,Very_High_Income,male_popul,female_pop,total_popu,no_of_hous)))
#Regression
X = np.array([list(Very_High_Income), list(Very_Low_Income), list(Low_Income), list(Medium_Income), list(High_Income)]).transpose()
Y1 = list(CURR_BILL)
Y2 = list(BROADBAND_BILL)
Y3 = list(PSTN_BILL)
Y4 = list(IPTV_BILL)

regr = linear_model.LinearRegression()

XX = np.array([list(new_Very_High_Income), list(new_Very_Low_Income), list(new_Low_Income), list(new_Medium_Income), list(new_High_Income)]).transpose()

# for i in range(len(Y1)):
#     a1 = Y1[i]
#     a2 = name_displ[i]
#     sql = "UPDATE abbottabad_areas_v1 SET actual_revenue = %s WHERE name_displ = %s;"
#     # sql = "select * from abbottabad_areas WHERE name_displ = '"+predictionArea+"';"
#     print(sql)
#
#     resp = cur.execute(sql, (a1, a2))
#     myConnection.commit()
X_square = np.array([i**2 for i in X])
X_cube = np.array([i**3 for i in X])
X_four = np.array([i**4 for i in X])
X_five = np.array([i**5 for i in X])
X_six = np.array([i**6 for i in X])
X2 = np.concatenate((X,X_square,X_cube),axis=1)
X = X2
regr.fit(X,Y1)

for i in range(len(female_pop)):
    print (name_displ_1[i])

    predictionValue = regr.predict([X[i][:]])[0]
    predictionArea = name_displ_1[i]
    sql = "UPDATE abbottabad_areas_v1 SET scond_order = %s WHERE name_displ = %s;"
    # sql = "select * from abbottabad_areas WHERE name_displ = '"+predictionArea+"';"
    print(sql)

    resp = cur.execute(sql, (predictionValue, predictionArea))
    myConnection.commit()



#.............. Runtime Prediction ................../////////////////////////////////////////////////



# cur.execute(
#     "SELECT SUM(CASE WHEN abbottabad_points_demo.income_lev IN ('Very Low Income') THEN 1 ELSE 0 END) AS V_L_Income, "
#     "SUM(CASE WHEN abbottabad_points_demo.income_lev IN ('Low Income') THEN 1 ELSE 0 END) AS L_Income, "
#     "SUM(CASE WHEN abbottabad_points_demo.income_lev IN ('Medium Income') THEN 1 ELSE 0 END) AS M_Income, "
#     "SUM(CASE WHEN abbottabad_points_demo.income_lev IN ('High Income') THEN 1 ELSE 0 END) AS H_Income, "
#     "SUM(CASE WHEN abbottabad_points_demo.income_lev IN ('Very High Income') THEN 1 ELSE 0 END) AS V_H_Income, "
#     "male_popul, female_pop, total_popu, name_displ "
#     "from abbottabad_areas_demo, abbottabad_points_demo "
#     "WHERE ST_Contains(abbottabad_areas_demo.geom_4326,abbottabad_points_demo.geom_4326) "
#     "GROUP BY name_displ, male_popul, female_pop, total_popu, name_displ")
# # features
# RT_Very_Low_Income = []
# RT_Low_Income = []
# RT_Medium_Income = []
# RT_High_Income = []
# RT_Very_High_Income = []
# RT_male_popul = []
# RT_female_pop = []
# RT_total_popu = []
# RT_name_displ_1 = []
#
#
# for firstname in cur.fetchall():
#     RT_Very_Low_Income.append(firstname[0])
#     RT_Low_Income.append(firstname[1])
#     RT_Medium_Income.append(firstname[2])
#     RT_High_Income.append(firstname[3])
#     RT_Very_High_Income.append(firstname[4])
#     RT_male_popul.append(firstname[5])
#     RT_female_pop.append(firstname[6])
#     RT_total_popu.append(firstname[7])
#     RT_name_displ_1.append(firstname[9])
#
# X = np.array([list(RT_Very_High_Income), list(RT_Very_Low_Income), list(RT_Low_Income), list(RT_Medium_Income), list(RT_High_Income)]).transpose()
#
#
# for i in range(len(RT_female_pop)):
#     print (RT_name_displ_1[i])
#
#     predictionValue = regr.predict([X[i][:]])[0]
#     predictionArea = RT_name_displ_1[i]
#     sql = "UPDATE abbottabad_areas_demo SET predicted = %s WHERE name_displ = %s;"
#     # sql = "select * from abbottabad_areas WHERE name_displ = '"+predictionArea+"';"
#     print(sql)
#
#     resp = cur.execute(sql, (predictionValue, predictionArea))
#     myConnection.commit()

###........................../////////////////////////////////............................


    # resp = cur.execute(sql)
    # print(resp)
# "UPDATE abbottabad_areas "
#             "SET predicted = " + int(regr.predict([X[i][:]])[0]) +
#             " WHERE name_displ = '" + new_name_displ_1[i] + "';"

# UPDATE table_name
# SET column1 = value1, column2 = value2, ...
# WHERE condition;


myConnection.close()