import csv
import chardet
import pandas as pd
with open('./DPWise_Data_1905.csv',errors='ignore') as csvfile:
    readCSV = csv.DictReader(csvfile)
    i = 0;
    DC_LATITUDE = ['DC_LATITUDE']
    DC_LONGITUDE = ['DC_LONGITUDE']
    DP_LATITUDE= ['DP_LATITUDE']
    DP_LONGITUDE= ['DP_LONGITUDE']
    DP_STATUS= ['DP_STATUS']
    SINGLE_PLAY= ['SINGLE_PLAY']
    DOUBLE_PLAY= ['DOUBLE_PLAY']
    TRIPLE_PLAY= ['TRIPLE_PLAY']
    EQUAL_4MB= ['EQUAL_4MB']
    EQUAL_10MB= ['EQUAL_10MB']
    EQUAL_20MB= ['EQUAL_20MB']
    GREATER_20MB= ['GREATER_20MB']
    UN_KNOWN= ['UN_KNOWN']
    CURR_BILL= ['CURR_BILL']
    PSTN_BILL= ['PSTN_BILL']
    BROADBAND_BILL= ['BROADBAND_BILL']
    IPTV_BILL= ['IPTV_BILL']
    OTHER_BILL= ['OTHER_BILL']
    PSTN_USAGE_MIN= ['PSTN_USAGE_MIN']
    MOB_MIN= ['MOB_MIN']
    INTL_MIN= ['INTL_MIN']
    BB_DOWNLOAD_GB= ['BB_DOWNLOAD_GB']
    BB_UPLOAD_GB= ['BB_UPLOAD_GB']
    final = []

    for row in readCSV:
        DC_LATITUDE.append(row['DC_LATITUDE'])
        DC_LONGITUDE.append(row['DC_LONGITUDE'])
        DP_LATITUDE.append(row['DP_LATITUDE'])
        DP_LONGITUDE.append(row['DP_LONGITUDE'])
        DP_STATUS.append(row['DP_STATUS'])
        SINGLE_PLAY.append(row['SINGLE_PLAY'])
        DOUBLE_PLAY.append(row['DOUBLE_PLAY'])
        TRIPLE_PLAY.append(row['TRIPLE_PLAY'])
        EQUAL_4MB.append(row['EQUAL_4MB'])
        EQUAL_10MB.append(row['EQUAL_10MB'])
        EQUAL_20MB.append(row['EQUAL_20MB'])
        GREATER_20MB.append(row['GREATER_20MB'])
        UN_KNOWN.append(row['UN_KNOWN'])
        CURR_BILL.append(row['CURR_BILL'])
        PSTN_BILL.append(row['PSTN_BILL'])
        BROADBAND_BILL.append(row['BROADBAND_BILL'])
        IPTV_BILL.append(row['IPTV_BILL'])
        OTHER_BILL.append(row['OTHER_BILL'])
        PSTN_USAGE_MIN.append(row['PSTN_USAGE_MIN'])
        MOB_MIN.append(row['MOB_MIN'])
        INTL_MIN.append(row['INTL_MIN'])
        BB_DOWNLOAD_GB.append(row['BB_DOWNLOAD_GB'])
        BB_UPLOAD_GB.append(row['BB_UPLOAD_GB'])

       # final.append([DC_LATITUDE, DC_LONGITUDE,DP_LATITUDE,DP_LONGITUDE,DP_STATUS,SINGLE_PLAY,DOUBLE_PLAY,TRIPLE_PLAY,EQUAL_4MB,EQUAL_10MB,EQUAL_20MB,GREATER_20MB,UN_KNOWN,CURR_BILL,PSTN_BILL,BROADBAND_BILL,IPTV_BILL,OTHER_BILL,PSTN_USAGE_MIN,MOB_MIN,INTL_MIN,BB_DOWNLOAD_GB,BB_UPLOAD_GB])
final  = [DC_LATITUDE, DC_LONGITUDE,DP_LATITUDE,DP_LONGITUDE,DP_STATUS,SINGLE_PLAY,DOUBLE_PLAY,TRIPLE_PLAY,EQUAL_4MB,EQUAL_10MB,EQUAL_20MB,GREATER_20MB,UN_KNOWN,CURR_BILL,PSTN_BILL,BROADBAND_BILL,IPTV_BILL,OTHER_BILL,PSTN_USAGE_MIN,MOB_MIN,INTL_MIN,BB_DOWNLOAD_GB,BB_UPLOAD_GB]

with open('./new.csv','w') as file:
    writeCSV = csv.writer(file)
    writeCSV.writerows(map(list, zip(*final)))

print (DC_LATITUDE[122586])


#
# # with open('./DPWise_Data_1905 (copy).csv', 'rb') as f:
# #     result = chardet.detect(f.read())  # or readline if the file is large
#
#
# fil = pd.read_csv('DPWise_Data_1905 (copy).csv')
#
# print (fil.isnull().sum())